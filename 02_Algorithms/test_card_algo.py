import unittest

from card_algo import locate_card


class TestCardAlgo(unittest.TestCase):
  def test_locate_card(self):
    '''default test case'''
    cards = [ 13, 12, 10, 8, 5, 3, 1 ]
    query = 10
    position = 2
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_middle(self):
    '''query is in middle of cards'''
    cards = [ 13, 12, 10, 8, 5, 3, 1 ]
    query = 8
    position = 3
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_first(self):
    '''query is first card'''
    cards = [ 10, 5, 2, 0 ]
    query = 10
    position = 0
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_last(self):
    '''query is last card'''
    cards = [ 3, -1, -9, -127]
    query = -127
    position = 3
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_single(self):
    '''cards is single element'''
    cards = [ 69 ]
    query = 69
    position = 0
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_not_found(self):
    '''query is not in cards'''
    cards = [ 13, 12, 10, 8, 5, 3, 1 ]
    query = 69
    position = -1
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_empty(self):
    '''cards is empty'''
    cards = []
    query = 69
    position = -1
    self.assertEqual(locate_card(cards, query), position)
  def test_locate_card_multiple(self):
    '''query is found multiple times in cards'''
    cards = [ 128, 128, 64, 32, 32, 32, 16, 2, 1 ]
    query = 32
    position = 3
    self.assertEqual(locate_card(cards, query), position)
