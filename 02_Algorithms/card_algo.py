'''
Alice hat einige Karten auf denen Zahlen stehen. Sie sortiert die Karten in absteigender Reihenfolge, 
und legt diese in einer Reihe mit der Schrift unten auf den Tisch.
Nun soll Bob die Karte mit einer, von Alice vorgegebenen Zahl finden UND dabei so wenig falsche
Karten wie möglich aufzudecken.

Schreiben Sie eine Helfer Funktion für den armen Bob.
'''
def locate_card(cards, query):
# 1. Create: variable `position` = 0
  position = 0
# 2. Loop Check: number at index of position in cards == query
  while True:
    if cards[position] == query:
#   3. If True: return `position`
      return position
#   4. If False: Increment `position` by 1 and repeat
    position += 1
# 5. If `query` not found: return -1
    if position == len(cards):
      return -1

cards = [ 13, 12, 10, 8, 5, 3, 1 ] # Input
query = 10 # Input
position = 2 # Output


res = locate_card(cards, query)
print(res)
