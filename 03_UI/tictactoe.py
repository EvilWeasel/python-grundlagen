from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QLabel
from PyQt5 import uic
import sys


class UI(QMainWindow):
  def __init__(self):
    # Call to the parent-constructor
    super(UI, self).__init__()

    # Load our UI-File
    uic.load_ui("tictactoe.ui", self)

    # Define a Counter-Variable to keep track of turns
    self.counter = 0

    # Define Widgets
    self.button_reset = self.findChild(QPushButton, "pushButton_reset")
    self.button_1 = self.findChild(QPushButton, "pushButton_1")
    self.button_2 = self.findChild(QPushButton, "pushButton_2")
    self.button_3 = self.findChild(QPushButton, "pushButton_3")
    self.button_4 = self.findChild(QPushButton, "pushButton_4")
    self.button_5 = self.findChild(QPushButton, "pushButton_5")
    self.button_6 = self.findChild(QPushButton, "pushButton_6")
    self.button_7 = self.findChild(QPushButton, "pushButton_7")
    self.button_8 = self.findChild(QPushButton, "pushButton_8")
    self.button_9 = self.findChild(QPushButton, "pushButton_9")
    self.label = self.findChild(QLabel, "label")

    # Click Button
    self.button_reset.clicked.connect(lambda: self.clicker(self.button_reset))
    self.button_1.clicked.connect(lambda: self.clicker(self.button_1))
    self.button_2.clicked.connect(lambda: self.clicker(self.button_2))
    self.button_3.clicked.connect(lambda: self.clicker(self.button_3))
    self.button_4.clicked.connect(lambda: self.clicker(self.button_4))
    self.button_5.clicked.connect(lambda: self.clicker(self.button_5))
    self.button_6.clicked.connect(lambda: self.clicker(self.button_6))
    self.button_7.clicked.connect(lambda: self.clicker(self.button_7))
    self.button_8.clicked.connect(lambda: self.clicker(self.button_8))
    self.button_9.clicked.connect(lambda: self.clicker(self.button_9))

    # Show the App
    self.show()

  # Check for Wins from both players
  def checkWin(self, a, b, c):
    # Change colors to indicate win
    a.setStyleSheet('QPushButton {color: red;}')
    b.setStyleSheet('QPushButton {color: red;}')
    c.setStyleSheet('QPushButton {color: red;}')

    # Add Win-Label
    self.label.setText(f"{a.text()} Wins!")

    # Disable Board until reset
    self.disable()

  def getButtonList(self):
    return [
      self.button_1,
      self.button_2,
      self.button_3,
      self.button_4,
      self.button_5,
      self.button_6,
      self.button_7,
      self.button_8,
      self.button_9,
    ]

  def disable(self):
    # List for all Buttons
    button_list = self.getButtonList()

    # Reset all buttons
    for b in button_list:
      b.setEnabled(False)

  def clicker(self, b):
    # Check who's turn it is with UI.counter
    # Set mark according to players-turn
    if self.counter % 2 == 0:
      mark = "X"
      self.label.setText("O's Turn")
    else:
      mark = "O"
      self.label.setText("X's Turn")

    # Set text for tic-tac-toe-field and disable it
    b.setText(mark)
    b.setEnabled(False)

    # Increment Counter
    self.counter += 1

    # Check if game is over
    self.checkWin()

  # Reset Game
  def reset(self):
    button_list = self.getButtonList()
    for b in button_list:
      # Reset Button Color && Text && Enable
      b.setText("")
      b.setEnabled(True)
      b.setStyleSheet('QPushButton {color: #797979;}')

    # Reset Turn-Label
    self.label.setText("X Goes First!")

    # Reset UI.counter
    self.counter = 0


# Init
app = QApplication(sys.argv)
UIWindow = UI()
app.exec_()
