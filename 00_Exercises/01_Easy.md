# Difficulty: Easy

## Exercise 1: Calculator

Write a calculator-programm, using the console to prompt the user for input and
choosing the operation.

The calculator should allow the user to at least do the four basic calculations:

- Add
- Subtract
- Multiply
- Divide

## Exercise 2: Check if divisible

Iterate through a given list of numbers and print all numbers which are divisible
by 5.

The List may look something like this: [10, 20, 30, 41, 132]

**Bonus-Points:** let the user decide an arbitrary ammount of numbers and use those
for the calculation
