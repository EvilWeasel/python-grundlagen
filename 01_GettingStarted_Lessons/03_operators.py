# Arithmetic Operators
'''
+ Addition
- Subtraction
* Multiplication
/ Division
% Modulo
** Exponent
// Floor Division

Additional Hackery eg.
value**(1/nth_root) = nth_root of value
x**(1/y) = y'th root of x
'''
def calculate(x,y, operator):
  match operator:
    case '+':
      return x + y
    case '-':
      return x - y
    case '*':
      return x * y
    case '/':
      return x / y
    case '%':
      return x % y
    case '**':
      return x ** y
    case '//':
      return x // y
    case _:
      return f"Invalid operator: {operator}"

# Assignment Operators (see arithmatic operators for more info)
'''
x = 0
x += 1
x -= 1
x *= 1
x /= 1
x %= 1
x **= 1
x //= 1
'''
def assignment_operators():
  for i in range(0,10000):
    if i < 10000:
      i += 1
    print(i)
  for j in range(9999,0,-1):
    if j > 0:
      j -= 1
    print(j)

# Logical Operators
'''
and ==> true and false = false
or ==> true or false = true
not ==> not true = false
'''
def collision_detection(entity_position = {"x":0,"y":0}, obstacle_position = {"x":0,"y":0}):
  # below you see a way to do multi-line statements (note, that the if statement extends up to the colon)
  if entity_position["x"] == obstacle_position["x"] \
    and entity_position["y"] == obstacle_position["y"]:
    return True
  else:
    return False

# Identity Operators
'''
is ==> true if both operands are the same object
is not ==> true if both operands are not the same object
'''
def identity_operators():
  a = [1,2,3]
  b = [1,2,3]
  c = a
  # following is false, because a and b may have the same value, but are different objects
  print(a is b)
  # following is true, because a and c are the same object, with diffrent pointers
  print(a is c)

# Membership Operators
'''
in ==> true if the value is in the sequence
not in ==> true if the value is not in the sequence
'''
def membership_operators():
  a = [1,2,3]
  # following is true, because 1 is in the list
  print(1 in a)
  # following is true, because 64 is not in the list
  print(64 not in a)

# Bitwise Operators
'''
& ==> bitwise AND
| ==> bitwise OR
^ ==> bitwise XOR
~ ==> bitwise NOT
<< ==> bitwise left shift
>> ==> bitwise right shift
'''
def bitwise_operators():
  a = 60 # 60 = 0011 1100
  b = 13 # 13 = 0000 1101
  
  # bitwise AND
  c = a & b; # 12 = 0000 1100
  print(c)
  # bitwise OR
  c = a | b; # 61 = 0011 1101
  print(c)
  # bitwise XOR
  c = a ^ b; # 49 = 0011 0001
  print(c)
  # bitwise NOT
  c = ~a; # -61 = 1100 0011
  print(c)
  # bitwise left shift
  c = a << 2; # 240 = 1111 0000
  print(c)
  # bitwise left shift by 1
  c = a << 1; # 120 = 0111 1000
  print(c)
  # bitwise right shift
  c = a >> 2; # 15 = 0000 1111
  print(c)


bitwise_operators()
