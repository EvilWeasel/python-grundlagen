# lets create a parent class to inherit from
class Animal:
  def eat(self):
    print("Eating")

class Dog(Animal):
  def bark(self):
    print("Woof!")
    
class Cat(Animal):
  def meow(self):
    print("Meow!")

dog1 = Dog()
dog1.bark()
# this works, because the eat method is defined in the Animal class, and the Dog class inherits from the Animal class
dog1.eat()

cat1 = Cat()
cat1.meow()
# again, because cat also inherits from the Animal class, cat can call the eat method
cat1.eat()


# Example creating a Polygon class
'''
Definition:
  A polygon (Vieleck) is a plane figure that is bounded by a finite number of straight line segments.
  A Triangle (Dreieck) is a polygon with 3 sides and 3 vertices.
  A Quadrilateral (Rechteck) is a polygon with 4 sides and 4 vertices.
'''
class Polygon:
  # this is the constructor for our parent class
  def __init__(self, sides) -> None:
    self.sides = sides
    
  def display_info(self):
    print("A polygon is a two-dimensional shape with straight lines.")
  
  def get_perimeter(self):
    return sum(self.sides)

class Triangle(Polygon):
  # We don't need to define the constructor here, because we are inheriting from the Polygon class
  # and the parent constructor will be implicitly called
  
  # we can override the display_info method of the parent class
  def display_info(self):
    print("A triangle is a polygon with 3 sides and 3 vertices.")
    
class Quadrilateral(Polygon):
    
  # same as before, we can override the display_info method of the parent class
  def display_info(self):
    print("A quadrilateral is a polygon with 4 sides and 4 vertices.")


t1 = Triangle([3, 4, 5])
print("The perimeter of t1 is: ", t1.get_perimeter())
t1.display_info()

q1 = Quadrilateral([3, 4, 5, 6])
print("The perimeter of q1 is: ", q1.get_perimeter())
q1.display_info()
