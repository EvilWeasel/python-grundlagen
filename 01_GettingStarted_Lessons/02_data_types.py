# Overview
'''
Python has a number of primitive data types you are familiar with.
There are also a lot of data types you know from other languages which are missing in Python.

* Numbers
  - integers ==> int = 42
  - floats ==> float = 42.0
  - complex numbers ==> complex = 1j
* Text
  - strings ==> str = 'hello'
* Logical
  - booleans ==> bool = True
* Collections
  - lists ==> list = [1, 2, 3]
  - tuples ==> tuple = (1, 2, 3)
  - sets ==> set = {1, 2, 3}
  - dictionaries ==> dict = {'key': 'value'}
* None

Can you name some, which are missing?

'''
from ast import arg


def get_data():
  name = input('What is your name? ')
  age = int(input('How old are you? '))
  is_of_legal_age = False if age < 18 else True
  data = {"Name": name, "Legal": is_of_legal_age}
  return data

# Numbers
'''
You don't neccessarily need to cast to do mathematical operations on two variables.
Python does a similar type-coercion to javascript, using the following rules:
int -> float -> complex

!!! Important !!!
Complex numbers may not used with comparing operators. (eg. 1j == 1) ==> Raises TypeError
'''
def casting_works():
  print(int(1.333), float(1337), complex(42))

def casting_fails():
  # basically, don't try to cast complex numbers to int or float
  print(int(4j+3), float(4j+3))


# Math-Module
'''
The math module contains a number of functions and constants which can be used,
to perform mathematical operations.

Constants:
  * pi
  * e
  * infinity
  * nan
  * tau

Some useful functions:
  * ceil(x) ==> round up
  * floor(x) ==> round down
  * sqrt(x) ==> square root
  * fsum(collection) ==> sums all elements of collection
  * gcd(x, y) => greatest common divisor
'''
def more_math():
  import math
  

  PI = math.pi
  E = math.e
  constants = (PI, E)
  numbers = (9, 180)
  
  print(f"{PI} hoch {E} = {PI**E}")
  print(f"Die Quadratwurzel von {numbers[0]} ist {math.sqrt(numbers[0])}")
  print(f"Die Summe aller Zahlen in {numbers} ist {math.fsum(numbers)}")
  print(f"Der groesste gemeinsame Teiler von {numbers} ist {math.gcd(numbers[0], numbers[1])}")


# Strings
'''
* use "" or '' to define a string
* strings are immutable
* strings are sequences of characters, which in python are represented by the str class
* strings surrounded by tripple quotes are called docstrings (or multiline-docstrings)
* strings may be concatenated with the '+' operator
* strings may be indexed with the '[]' operator
* strings may be formatted using the format() method to format strings or the f"" string-literal
''' 
def strings_as_collections():
  my_string = "Hello World!"
  for char in my_string:
    print(char)

def docstrings():
  ascii_art = '''
                       _                     _   _                 
                      | |                   | | | |                
 _ __ ___   ___  _ __ | |_ _   _ _ __  _   _| |_| |__   ___  _ __  
| '_ ` _ \ / _ \| '_ \| __| | | | '_ \| | | | __| '_ \ / _ \| '_ \ 
| | | | | | (_) | | | | |_| |_| | |_) | |_| | |_| | | | (_) | | | |
|_| |_| |_|\___/|_| |_|\__|\__, | .__/ \__, |\__|_| |_|\___/|_| |_|
                            __/ | |     __/ |                      
                           |___/|_|    |___/                       
  '''
  print(ascii_art)
  
def format_string():
  string1 = 'Hello'
  string2 = 'World'

  format1 = '{} {}'.format(string1, string2)
  format2 = '{1} {0}'.format(string1, string2)
  format3 = f"{string1} {string2}"
  
def format_num_string():
  num = 42.1337
  with_decimals = f"{num:.2f}"
  with_decimals_alt = "{:.2f}".format(num)
  print(with_decimals, with_decimals_alt)
  
# More string-methods
'''
* len(string) ==> returns the length of the string
* string.upper() ==> returns a copy of the string with all characters in uppercase
* string.lower() ==> returns a copy of the string with all characters in lowercase
* string.capitalize() ==> returns a copy of the string with the first character in uppercase
* string.title() ==> returns a copy of the string with the first character of each word in uppercase
* string.replace(old, new) ==> returns a copy of the string with all occurrences of old replaced by new
* string.strip() ==> returns a copy of the string with all leading and trailing whitespace removed
* string.split(sep) ==> returns a list of the string split into substrings at each occurrence of sep
''' 
def string_methods():
  target_string = ""
  target_list = []

  original_string = "This Sentence is false, determined by me."
  string_in_uppercase = original_string.upper()
  string_in_lowercase = original_string.lower()
  string_replaced = original_string.replace("false", "true")
  string_list = string_replaced.split(",")
  for substring in string_list:
    substring.strip()
    print(substring.capitalize())


def check_bool():
  arguments = [
    "True",
    "False",
    1,
    -5,
    34.5,
    16j,
    (),
    [],
    {},
  ]
  for a in arguments:
    print(f"{a} is {bool(a)}")

