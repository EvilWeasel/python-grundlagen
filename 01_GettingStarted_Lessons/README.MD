# Python Basics

## Was ist Python?

Python ist eine performante, Objekt-Orientierte Programmiersprache mit Fokus auf Erweiterbarkeit und "Ease-of-Use".

[python.org]: https://www.python.org/

Nennenswerte Features von Python:

- Einfacher Syntax
- Der Entwicklungsprozess ist einfacher und schneller als bei anderen Sprachen
  - Dadurch ist Python sehr beliebt bei der Entwicklung von Prototypen
- Pythons "Interactive"-Mode lässt es zu, einzelne Code-Artefakte schnell zu testen
- Kann in einem Programm eingebettet werden, um ein programmierbares Interface zu bieten
- Läuft auf auf jedem System (Windows, OSX, Linux, Unix, Android und IOS)

Features der Sprache:

- OOP-Support mit Klassen und Mehrfach-Vererbung
- Automatische Speicherverwaltung

## Installation

**WICHTIG!**
Falls Sie Python über den GUI-Installer installieren möchten, stellen Sie sicher, dass Python zum "Path" hinzugefügt wird.
Ansonsten können wir nicht einfach von jedem Verzeichnis aus auf Python zugreifen, sondern müssen immer den Pfad zur Binary angeben.

```shell
C:\Users\<UserName>\AppData\Local\Programs\Python\Python39.exe .\program.py
```

... ziemlich nervig ^^

Prüfen ob Python bereits installiert ist:

```powershell
python --version ||
py --version
```

Falls nicht, lässt es sich ganz einfach über [winget] nachinstallieren.

```powershell
winget install python.python.3
```

Alternativ ist Python auch im MSStore vorhanden.

## IDE - Entwicklungsumgebung

1. Visual Studio Code

- `winget install microsoft.visualstudiocode`

2. PyCharm

- `winget install jetbrains.pycharm.community`

3. Basically jedes Textbearbeitungsprogramm

[winget]: https://docs.microsoft.com/en-us/windows/package-manager/winget/
