# Classes

class Cat:
  pass

cat1 = Cat()
cat2 = Cat()

# you can add attributes to an instance of a class, after you have created it
cat1.name = "Mr. Whiskers"
print(f"cat1 name: {cat1.name}")
try:
  print(f"cat2 name: {cat2.name}")
except AttributeError as ex:
  print(f"ERROR: cat2 has no attribute \"{ex.name}\"")
cat2.name = "Mrs. Whiskers"
print(f"cat2 name: {cat2.name}")


class Dog:
  '''
  this is a constructor, where the "self" parameter is the instance of the class
  in python, when defining a instanced method, the first parameter is always "self"
  '''
  def __init__(self, name):
    self.name = name
  
  # static methods are methods that are not associated with a specific instance, therefore no "self" parameter
  def bark():
    print("Woof!")

  # instance methods are methods that are associated with a specific instance, therefore the "self" parameter is required
  def eat(self, food: str):
    print(f"{self.name} is eating {food}")
    
dog1 = Dog("Fido")

# dog1.bark() throws error, because bark is a static method and needs to be called on the class itself
Dog.bark()
dog1.eat("treats")

class Person:
  '''
  This is a demo of docstrings, which will get printed out, when you call the __doc__ method on the class
  For more information on creating documentation with python, see: https://peps.python.org/pep-0257/
  '''
  def __init__(self, name, age):
    self.name = name
    self.age = age
  
  def birthday(self):
    self.age += 1
  
  def __str__(self) -> str:
    return f"{self.name} is {self.age} years old"


person1 = Person("John", 30)
print(person1.__str__())
print(person1.__repr__())
print(person1.__doc__)


# One more example
class Complex:
  '''
  complex numbers have two parts:
    1. a real part => 42
    2. an imaginary part => 4i
  '''
  def __init__(self, real, imag) -> None:
    self.real = real
    self.imag = imag
  
  def add(self, number: 'Complex') -> 'Complex':
    real = self.real + number.real
    imag = self.imag + number.imag
    result = Complex(real, imag)
    return result

  def __str__(self) -> str:
    return f"{self.real} + {self.imag}i"

  
n1 = Complex(5, 6)
n2 = Complex(-4, 2)
result = n1.add(n2)
print(result)

# fun little side-note, you can view all 'members' of a class with the dir() function
print(dir(Complex))
# Note the difference between calling dir on an instance and calling dir on a class
print(dir(n1))

# view id of an instance of anobject
print(f"id of n1: {id(n1)}\nid of n2: {id(n2)}")
