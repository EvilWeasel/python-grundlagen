def sneaking_snake_case():
  pass

# Where are my curly braces?
'''
This is a multi-line comment.
Code-blocks are depending on the indentation.
'''
def indent_example():
  user_input = input("Geben Sie eine Zahl ein: ")
  if not user_input.isdigit():
    try:
      print(user_input + " ist keine Zahl")
    except Exception as err:
      print("Fehler: " + err)
    finally:
      print("Dieser Text wird in jeden Fehler-Fall ausgegeben")
    print()
  else:
    print("Das ist eine Zahl")
    
# Variables
'''
Python is, as apposed to C#, a dynamically-typed language.
There also is no need to declare variables.
Variables get defined when they are assigned a value.
'''
def variables(n = 42):
  i = "This is a string"
  c = 3.14159
  e = False
  return [n,i,c,e]

# Type-hinting
'''
Since python 3.5, you can use type-hinting to specify the type of a variable.
This is useful for debugging and for code-readability.
''' 
def type_hinting(n: int = 42):
  i: str = "This is a string"
  c: float = 3.14159
  e: bool = False
  return [n,i,c,e]

# Casting- and type-functions
'''
To cast a variable to a different type, you can use the following functions:
'''
def casting(f = "42"):
  u = int(f)
  n = [type(f), type(u)]
  return [f,u,n]


# Naming conventions
'''
!!! IMPORTANT !!!
your filenames in real projects should NOT start with a number
==> this will create silly errors when trying to import the file as a module
* Variables: snake_case
* Functions: snake_case
* Methods: snake_case
* Modules: snake_case
* Classes: PascalCase
* Constants: SCREAMING_SNAKE_CASE
'''
def naming():
  import my_cool_module
  class MyCoolClass:
    this_is_a_static_variable = "This is a static-variable"
    def __init__(self):
      # We get to this later on...
      THIS_IS_A_CONSTANT = 42
      self.this_is_a_instance_variable = "This is a instance-variable"


    
# Our main function, which in python is not automatically called
def main():
  # Function call
  indent_example()

# Thats why these 2 lines are needed
'''
If the script is executed directly, the `__name__` variable will be set to `__main__`
main() is called
If we import the script as a module, the `__name__` variable will be set to
the filename of the module
'''
if __name__ == '__main__':
  main()
