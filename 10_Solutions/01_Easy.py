def ex1():
  print("My cool Calculator")
  print("Give me two numbers, and I'll do some math for you.")
  try:
    num1 = int(input("First number: "))
    num2 = int(input("Second number: "))
  except:
    print("Please enter a number.")
    exit()
  print("What do you want to do?")
  print("1. Add")
  print("2. Subtract")
  print("3. Multiply")
  print("4. Divide")
  try:
    choice = int(input("Enter a number: "))
  except:
    print("Please enter a number.")
    exit()
  match choice:
    case 1:
      print("Result: ", num1 + num2)
    case 2:
      print("Result: ", num1 - num2)
    case 3:
      print("Result: ", num1 * num2)
    case 4:
      print("Result: ", num1 / num2)
    # this is the default case in python syntax
    case _:
      print("Please enter a number between 1 and 4.")
    




def ex2():
  num_list = [10, 20, 33, 46, 55]
  print("Given list:", num_list)
  print('Divisible by 5:')
  for num in num_list:
    if num % 5 == 0:
      print(num)

def ex2_bonus():
  num_list = []
  print("Give me multiple numbers, one at a time. When you're done, simply press enter, without typing anything.")
  while True:
    user_input = input("Enter a number: ")
    if user_input == "":
      break
    num_list.append(int(user_input))
  if len(num_list) == 0:
    print("You didn't enter any numbers.")
  for num in num_list:
    if num % 5 == 0:
      print(num)

ex1()
